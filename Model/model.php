<?php 
    class Model {
        private $types;
        private $products;

        public function __construct() {
            $serverName = 'localhost';
            $userName = 'root';
            $password = '';
            $baseName = 'productlist';

            $this->DB = new DataBase($serverName, $userName, $password, $baseName);

            $this->types = ['DVD' => 'size', 'BOOK' => 'weight', 'FURNITURE' => 'dimension'];
        }

        public function select () {
            $this->DB->query('SELECT * FROM items');
            $result = $this->DB->getAll();

            $data = array();

            foreach($result as $value) {
                $className = ucfirst($value['type']);
                $data[$value['sku']] = new $className (
                    $value['sku'],
                    $value['name'],
                    $value['price'],
                    $value[$this->types[$value['type']]]
                );
            }
            return $data;
        }

        public function getProductFromBase($sku) {
            $this->DB->query("SELECT * FROM items WHERE sku=?", $sku);
            return $this->DB->getAll();
        }

        public function AddProduct($Product) {
            
            $this->DB->query("INSERT INTO `items` (`sku`, `name`, `price`, `type`, ". $this->types[$Product->getType()] .")". "VALUES" ."(?, ?, ?, ?, ?)",
                                $Product->getSku(), $Product->getName(), $Product->getPrice(), $Product->getType(),$Product->getOption());
            $this->DB->execute();
        }

        public function deleteProducts($sku) {
            $placeholders = implode(',',array_fill(0, count($sku), '?') );
            $this->DB->query("DELETE FROM items WHERE sku IN ($placeholders)" , ...$sku);
            $this->DB->execute();
        }
    }
    

?>