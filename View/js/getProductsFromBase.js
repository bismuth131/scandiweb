//for validation
async function getProductsFromBase() {
  const url = host + "requestProducts";
  const response = await fetch(url, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return await response.json();
}
