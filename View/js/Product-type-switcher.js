"use strict";

const typeSwitcher = document.getElementById("type-switcher");
const typeInsert = document.getElementById("type-insert");

const TYPE = new Type();
const DEFAULT = new Default();
const DVD = new Dvd();
const BOOK = new Book();
const FURNITURE = new Furniture();

const typeObj = {
  DEFAULT : DEFAULT,
  DVD : DVD,
  BOOK : BOOK,
  FURNITURE : FURNITURE,
}


typeSwitcher.addEventListener("change", function () {
  //option selection
  const type = typeSwitcher.value;
  //select elements, so when case "" deletes all existed elements from type-switcher
  const types = document.querySelectorAll(".type");

  typeObj[type].insertHtml();
  TYPE.addDescription(typeObj[type].description);

});



