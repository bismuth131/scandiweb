const addItemForm = document.querySelector(".itemData");

addItemForm.addEventListener("submit", function (e) {
  e.preventDefault();

  if (
    !skuValidation() ||
    !priceValidation() ||
    !typeProductsValidation() ||
    !addItemForm.checkValidity()
  )
    return false;

  const item = {
    sku : document.getElementById('sku').value,
    name : document.getElementById('Name').value,
    price : document.getElementById('price').value,
    type : document.getElementById('type-switcher').value,
    size : (document.getElementById('size') || '').value,
    weight : (document.getElementById('weight') || '').value,
    dimension : (document.getElementById('height') || '').value + 'X' +
                (document.getElementById('width') || '').value + 'X' +
                (document.getElementById('length') || '').value,
  }


  fetch(host + "addProduct", {
    method: "POST",
    mode: "same-origin",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(
        {item: item}
    ),
  })
    .catch(function (error) {
      console.error(error);
    });
});
