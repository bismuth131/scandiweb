function getItemstoProductlist (data) {

    const dataTypeObj = {
        DVD: ['size','MB'],
        BOOK: ['weight','KG'],
        FURNITURE: ['dimension','']
    }

    let allProducts= '';
    for (let i = 0; i < data.length; i++) {

        const infoProduct = data[i][dataTypeObj[[data[i].type]][0]] + dataTypeObj[[data[i].type]][1];

        const product = `<article class="product">
                            <input type="checkbox" class="checkbox" />
                            <p class="sku">${data[i].sku}</p>
                            <p class="name">${data[i].name}</p>
                            <p class="price">${data[i].price}$</p>
                            <p class="attribute">${infoProduct}</p>
                        </article>`;
        allProducts += product;
    }

    document.getElementsByClassName('product-list')[0].innerHTML = allProducts;
    addEventListenerToItems();
  }
