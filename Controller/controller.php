<?php


class Controller
{
    private $model;
    private $dataTypes;

    public function __construct() {
        $this->dataTypes = ['DVD' => ['size', 'is_numeric'], 'BOOK' => ['weight', 'is_numeric'] , 'FURNITURE' => ['dimension', 'trim']];
        $this->model = new Model();
    }

    public function showAlert ($alert) {
        header('Content-type:application/json');
        echo json_encode($alert);
        die();
    }

    public function requests() {
        $requestInfo = file_get_contents('php://input');
        $data = json_decode($requestInfo, true);

        if (isset($_REQUEST['q'])) {
            $requestType = $_REQUEST['q'];

            if ($requestType == 'addProduct') {

                if (!isset($data['item'])) {
                    $this->showAlert(array('error' => 'Missing argument error'));
                }

                $requiredElements = ['sku', 'name', 'price', 'type'];

                foreach ($requiredElements as $value) {
                    if (!isset($data['item'][$value]) || trim($data['item'][$value]) == '') {
                        $this->showAlert(array('error' => 'Missing argument error'));
                    }
                }

                $dataType = $data['item']['type'];

                if (!isset($data['item'][$this->dataTypes[$dataType][0]]) || !$this->dataTypes[$dataType][1]($data['item'][$this->dataTypes[$dataType][0]])) {
                    $this->showAlert(array('error' => 'Missing argument error'));
                }


                if (count($this->model->getProductFromBase($data['item']['sku'])) != 0) {
                    $this->showAlert(array('error' => 'Product with this SKU exists'));
                }

                $className = ucfirst($dataType);

                $Product = new $className($data['item']['sku'], $data['item']['name'], $data['item']['price'], $data['item'][ $this->dataTypes[$dataType][0] ]);

                $this->model->AddProduct($Product);
            }
            else if ($requestType == 'requestProducts') {
                $Data = $this->model->select();

                $productsList = [];

                foreach ($Data as $key => $value) {
                    $productsList[] = array(
                        'sku'       => $value->getSku(),
                        'name'      => $value->getName(),
                        'price'     => $value->getPrice(),
                        'type'      => $value->getType(),
                        'size'      => $value->getOption(),
                        'weight'    => $value->getOption(),
                        'dimension' => $value->getOption()
                    );
                }

                $this->showAlert($productsList);
            }
            else if($requestType == 'deleteProducts') {

                if (!isset($data['delete'])) {
                    $this->showAlert(array('error' => 'Missing argument error'));
                }

                $this->model->deleteProducts($data['delete']);

                //we need to select items left after deleting and pass it back to JS
                $Data = $this->model->select();

                $productsList = [];

                foreach ($Data as $key => $value) {
                    $productsList[] = array(
                        'sku'       => $value->getSku(),
                        'name'      => $value->getName(),
                        'price'     => $value->getPrice(),
                        'type'      => $value->getType(),
                        'size'      => $value->getOption(),
                        'weight'    => $value->getOption(),
                        'dimension' => $value->getOption()
                    );
                }

                $this->showAlert($productsList);

            }
            else {
                $this->showAlert(array('error' => 'Wrong request'));
            }
        }
        else {
            $this->showAlert(array('error' => 'Missing argument error'));
        }
    }
}